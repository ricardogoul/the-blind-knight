﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public GameObject player;

    Rigidbody body;

    public Text textY;
    public Text textX;
    public Text textZ;
    public Text textOrientantion;


    public float heading = 0;

    //public float averageHeading;

    //public int headingTicker = 0;

    // Use this for initialization
    void Start()
    {

        body = GetComponent<Rigidbody>();

        Input.gyro.enabled = true;
        Input.compass.enabled = true;
        //Input.location.Start();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButton("Fire1"))
        {
            body.velocity = player.transform.forward * 200 * Time.deltaTime;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            body.velocity = new Vector3(0, 0, 0);
        }

        
        //transform.rotation = Quaternion.Euler(0, -Input.compass.magneticHeading, 0);                                  
        
    }

    private void FixedUpdate()
    {

        //FindHeadingAverage();

        if(Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown || Input.deviceOrientation == DeviceOrientation.Unknown)
        {
            transform.Rotate(0, -Input.gyro.rotationRateUnbiased.y, 0);
        }
        else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            transform.Rotate(0, -Input.gyro.rotationRateUnbiased.x, 0);
        }
        else if (Input.deviceOrientation == DeviceOrientation.FaceUp || Input.deviceOrientation == DeviceOrientation.FaceDown)
        {
            transform.Rotate(0, -Input.gyro.rotationRateUnbiased.z, 0);
        }

        textX.text = (Input.gyro.rotationRateUnbiased.x * -1).ToString();
        textY.text = (Input.gyro.rotationRateUnbiased.y * -1).ToString();
        textZ.text = (Input.gyro.rotationRateUnbiased.z * -1).ToString();
        textOrientantion.text = Input.deviceOrientation.ToString();

        //if (Input.compass.trueHeading > 180)
        //{
        //    heading = Input.compass.trueHeading - 360;
        //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, heading, 0), 0.1f);
        //}
        //else
        //{
        //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, Input.compass.trueHeading, 0), 0.1f);
        //}
    }

    //void FindHeadingAverage ()
    //{

    //    if(headingTicker < 60)
    //    {
    //        headingTicker++;

    //        averageHeading += Input.compass.trueHeading;

    //    } else
    //    {
    //        heading = averageHeading/60;
    //        headingTicker = 0;

    //        Quaternion rot = transform.rotation;
    //        Vector3 eulerRot = new Vector3(0, heading, 0);
    //        rot.eulerAngles = eulerRot;
    //        transform.rotation = rot;

    //        textY.text = (heading.ToString());

    //    }

    //}

}
